﻿using System;

namespace Kata02.WebServices
{
    public class WebServiceACotacaoDolarAmericano
    {
        public double ObterCotacaoAtual(string moeda)
        {
            var random = new Random((int) DateTime.Now.Ticks);
            switch (moeda)
            {
                case "BRL": return 3.2d + random.NextDouble() * 0.5d;
                case "‎JPY": return 108.39d + random.NextDouble() * 30;
                case "EUR": return 4d + random.NextDouble() * 0.2;
                default: throw new NotSupportedException("Moeda não suportada pelo webservice");
            }
        }
    }
}