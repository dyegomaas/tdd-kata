﻿using System;
using System.Linq;

namespace Kata02.HistoricoCotacoes
{
    public class HistoricoCotacaoDolar
    {
        public double[] ObterHistoricoCotacao(int dias, string moeda)
        {
            var random = new Random((int) DateTime.Now.Ticks);
            return Enumerable.Repeat(1, dias)
                .Select(x =>
                {
                    var cotacao = ObterCotacaoExemplo(moeda);
                    var fator = random.NextDouble() > 0.5 ? 1 : -1;
                    return cotacao.CotacaoPadrao + random.NextDouble() * cotacao.VariacaoMedia * fator;
                })
                .ToArray();
        }
        
        private CotacaoExemplo ObterCotacaoExemplo(string moeda)
        {
            switch (moeda)
            {
                case "BRL": return new CotacaoExemplo(3.2d, .2d);
                case "‎JPY": return new CotacaoExemplo(108.39d, .25d);
                case "EUR": return new CotacaoExemplo(4d, .15d);
                default: throw new NotSupportedException("Moeda não encontrada no histórico");
            }
        }

        private class CotacaoExemplo
        {
            public double CotacaoPadrao { get; }
            public double VariacaoMedia { get; }

            public CotacaoExemplo(double cotacaoPadrao, double variacaoMedia)
            {
                CotacaoPadrao = cotacaoPadrao;
                VariacaoMedia = variacaoMedia;
            }
        }
    }
}