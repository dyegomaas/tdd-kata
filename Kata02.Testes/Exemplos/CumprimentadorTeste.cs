﻿using System;
using System.Collections.Generic;
using System.Linq;
using NSubstitute;
using NUnit.Framework;

namespace Kata02.Testes.Exemplos
{
    public class CumprimentadorTeste
    {
        [Test]
        public void CumprimentandoVariosUsuariosAleatorios()
        {
            ISorteadorUsuarios sorteador = new SorteadorUsuariosMuitoAleatorio();
            var cumprimentador = new Cumprimentador(sorteador);

            var cumprimentos = cumprimentador.CumprimentarTodos();

            foreach (var cumprimento in cumprimentos)
            {
                // usando um Sorteador Real não temos como saber que cumprimento foi gerado
                Console.WriteLine(cumprimento);
            }
        }
        
        [Test]
        public void CumprimentandoVariosUsuariosUsandoUmDummy()
        {
            var sorteador = new SorteadorBandidoDummy(); // aqui temos um exemplo de dummy feito manualmente
            var cumprimentador = new Cumprimentador(sorteador);

            var cumprimentos = cumprimentador.CumprimentarTodos();

            foreach (var cumprimento in cumprimentos)
            {
                Assert.AreEqual("Olá, caro cliente Bandido! Seja bem-vindo", cumprimento);
            }
        }
        
        [Test]
        public void CumprimentandoVariosUsuariosUsandoUmStub()
        {
            var sorteador = new SorteadorStub("Darth Vader"); // aqui temos um exemplo de stub
            var cumprimentador = new Cumprimentador(sorteador);

            var cumprimentos = cumprimentador.CumprimentarTodos();

            foreach (var cumprimento in cumprimentos)
            {
                Assert.AreEqual("Olá, caro cliente Darth Vader! Seja bem-vindo", cumprimento);
            }
        }
        
        [Test]
        public void CumprimentandoVariosUsuariosUsandoUmMock()
        {
            var sorteador = new SorteadorMock(); // aqui temos um exemplo de mock feito manualmente (sem framework)
            var cumprimentador = new Cumprimentador(sorteador);
            
            sorteador.Retorna("Sabrina"); // aqui configuramos o comportamento do nosso mock

            var cumprimentos = cumprimentador.CumprimentarTodos();

            foreach (var cumprimento in cumprimentos)
            {
                Assert.AreEqual("Olá, caro cliente Sabrina! Seja bem-vindo", cumprimento);
            }
        }
        
        [Test]
        public void CumprimentandoVariosUsuariosUsandoUmFake()
        {
            var sorteador = new SorteadorFake(); // aqui temos um exemplo de fake feito manualmente
            var cumprimentador = new Cumprimentador(sorteador);

            var cumprimentos = cumprimentador.CumprimentarTodos().ToList();

            foreach (var cumprimento in cumprimentos)
            {
                Assert.IsTrue(cumprimento.StartsWith("Olá, caro cliente"));
            }
        }
        
        [Test]
        public void CumprimentandoVariosUsuariosUsandoABibliotecaNSubstitute()
        {
            var sorteador = Substitute.For<ISorteadorUsuarios>();
            var cumprimentador = new Cumprimentador(sorteador);
            
            sorteador.Sortear().Returns("Sabrina"); // aqui configuramos o comportamento do nosso mock

            var cumprimentos = cumprimentador.CumprimentarTodos();

            foreach (var cumprimento in cumprimentos)
            {
                Assert.AreEqual($"Olá, caro cliente Sabrina! Seja bem-vindo", cumprimento);
            }
        }
    }

    /// <summary>
    /// Esta classe gera um cumprimento para cada usuário sorteado pelo <see cref="ISorteadorUsuarios"/>
    /// </summary>
    public class Cumprimentador
    {
        private readonly ISorteadorUsuarios _sorteador;
        private readonly Random _random;

        public Cumprimentador(ISorteadorUsuarios sorteador)
        {
            _sorteador = sorteador;
            _random = new Random();
        }

        public IEnumerable<string> CumprimentarTodos()
        {
            for (var i = 0; i < _random.Next(3, 5); i++)
            {
                var nomeSorteado = _sorteador.Sortear();
                yield return $"Olá, caro cliente {nomeSorteado}! Seja bem-vindo";
            }
        }
    }

    public class SorteadorBandidoDummy : ISorteadorUsuarios
    {
        public string Sortear()
        {
            return "Bandido";
        }
    }
    
    public class SorteadorStub : ISorteadorUsuarios
    {
        private readonly string _nome;

        public SorteadorStub(string nome)
        {
            _nome = nome;
        }

        public string Sortear()
        {
            return _nome;
        }
    }

    /// <summary>
    /// Mock da interface ISorteadorUsuarios, para facilitar testes do Cumprimentador
    /// </summary>
    public class SorteadorMock : ISorteadorUsuarios
    {
        private string _usuarioParaRetornar;

        public string Sortear()
        {
            return _usuarioParaRetornar;
        }

        public void Retorna(string usuario)
        {
            _usuarioParaRetornar = usuario;
        }
    }
    
    public class SorteadorFake : ISorteadorUsuarios
    {
        private static int _indice = 0;
        private readonly string[] _usuariosParaRetornar = {
            "Vader",
            "Luke",
            "Obi Wan"
        };

        public string Sortear()
        {
            var usuarioSorteado = _usuariosParaRetornar[_indice];
            _indice = (_indice + 1) % _usuariosParaRetornar.Length;
            return usuarioSorteado;
        }
    }

    /// <summary>
    /// Implementação do sorteador real, que inclui aleatoriedade e dificulta os testes das classes
    /// que dependem dele, como o Cumprimentador 
    /// </summary>
    public class SorteadorUsuariosMuitoAleatorio : ISorteadorUsuarios
    {
        private static readonly string[] Participantes = {
            "Mikael",
            "Wellington",
            "Rafael",
            "João",
            "Putin",
            "Maycon"
        };

        private readonly Random _random = new Random((int) DateTime.Now.Ticks);

        public string Sortear()
        {
            return Participantes[_random.Next(0, Participantes.Length)];
        }
    }

    public interface ISorteadorUsuarios
    {
        string Sortear();
    }
}