﻿using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;

namespace Calculadora.Testes
{
    public class CalculadoraExemploTeste
    {
        [Test]
        public void DeveSomarDoisOperandos() // o nome do método de teste deve deixar claro tanto a operação quanto o resultado esperados
        {
            // 1º etapa (opcional) - CONFIGURAÇÃO: aqui você configura o cenário que deseja testar
            var calculadora = new CalculadoraExemplo();
            calculadora.AdicionarOperando(2);
            calculadora.AdicionarOperando(5);
            //
            // ----------------------
            // 2ª etapa - AÇÃO: aqui você performa a ação sujeita ao teste
            //
            var soma = calculadora.Somar();
            //
            // ----------------------
            // 3ª etapa - ASSERÇÃO: aqui você garante que a saída obtida é a saída esperada 
            //
            Assert.AreEqual(7, soma, "deveria somar todos os operandos");
        }
    }

    public class CalculadoraExemplo
    {
        private readonly List<int> _operandos = new List<int>();

        public void AdicionarOperando(int operando)
        {
            _operandos.Add(operando);
        }

        public int Somar()
        {
            return _operandos.Sum();
        }
    }
}